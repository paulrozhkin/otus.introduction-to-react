import './App.css';
import Login from "./Login";
import {Col, Row, Container} from "react-bootstrap";

function App() {
    return (
        <div className="App">
            <Container className="h-100" fluid>
                <Row className="justify-content-center align-items-center h-100">
                    <Col xl={3} lg={4} md={6} sm={6} className="login-form">
                        <Login/>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default App;
