import {Form} from "react-bootstrap";
import axios from "axios";
import {useState} from "react";

function Login() {

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({})

    const setField = (field, value) => {
        setForm({
            ...form,
            [field]: value
        })

        // Check and see if errors exist, and remove them from the error object:
        if ( !!errors[field] ) setErrors({
            ...errors,
            [field]: null
        })
    }

    const findFormErrors = () => {
        const {email, password} = form
        const newErrors = {}
        // email errors
        if (!email || email === '') newErrors.email = 'cannot be blank!'
        else if (email.length > 50) newErrors.email = 'email is too long!'
        // password errors
        if (!password || password === '') newErrors.password = 'cannot be blank!'
        else if (password.length < 6) newErrors.password = 'password is too short!'

        return newErrors
    }

    function handleSubmit(event) {
        event.preventDefault();

        // get our new errors
        const newErrors = findFormErrors()

        // Conditional logic:
        if (Object.keys(newErrors).length > 0) {
            // We got errors!
            setErrors(newErrors)
        } else {
            axios.post(`/api/login`, {
                email: form.email, password: form.password
            })
                .then(res => {
                    //const persons = res.data;
                    alert(JSON.stringify(res.data))
                })
                .catch(function (error) {
                    if (error.response) {
                        // The request was made and the server responded with a status code
                        // that falls out of the range of 2xx
                        console.log(error.response.data);
                        console.log(error.response.status);
                        console.log(error.response.headers);

                        if (error.response.status === 400) {
                            alert("Invalid user. Use `eve.holt@reqres.in`")
                        } else {
                            alert(JSON.stringify(error.response.data))
                        }
                    } else if (error.request) {
                        // The request was made but no response was received
                        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                        // http.ClientRequest in node.js
                        console.log(error.request);
                    } else {
                        // Something happened in setting up the request that triggered an Error
                        console.log('Error', error.message);
                    }
                });
        }
    }

    return (
        <Form onSubmit={handleSubmit}>
            <h3>Sign In</h3>

            <Form.Group>
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" onChange={e => setField('email', e.target.value)}
                              isInvalid={!!errors.email}/>
                <Form.Control.Feedback type='invalid'>
                    {errors.email}
                </Form.Control.Feedback>
            </Form.Group>

            <Form.Group>
                <label>Password</label>
                <Form.Control type="password" placeholder="Enter password"
                              onChange={e => setField('password', e.target.value)}
                              isInvalid={!!errors.password}/>
                <Form.Control.Feedback type='invalid'>
                    {errors.password}
                </Form.Control.Feedback>
            </Form.Group>

            <div className="btn-group">
                <button type="submit" className="btn btn-primary btn-block">Submit</button>
            </div>

            <p className="forgot-password text-right">
                Forgot <a href="#">password?</a>
            </p>
        </Form>
    );
}

export default Login;
